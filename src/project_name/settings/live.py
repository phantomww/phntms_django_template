from {{ project_name }}.settings import *

import os

SECRET_KEY = os.environ.get('SECRET_KEY', None)

DEBUG = False
TEMPLATE_DEBUG = False

ALLOWED_HOSTS = []

# Database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ project_name }}',
        'USER': '{{ project_name }}',
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD', None),
        'HOST': 'localhost',
    }
}
