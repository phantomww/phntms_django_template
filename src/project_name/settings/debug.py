import platform
from {{ project_name }}.settings import *

DEBUG = True
TEMPLATE_DEBUG = True

# Database

if platform.system() == 'Darwin':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, '../../../{{project_name}}.sqlite3'),
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': '',
            'USER': '',
            'PASSWORD': '',
            'HOST': 'localhost',
        }
    }

MIDDLEWARE_CLASSES += (
    #'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INSTALLED_APPS += (
    #'debug_toolbar',
)

INTERNAL_IPS = ('127.0.0.1',)
